package midiPlayer;

import javax.sound.midi.*;

public class TestReceiver implements Receiver {

	@Override
	public void close() {
		
	}

	@Override
	public void send(MidiMessage message, long timeStamp) {
		//System.out.print("@"+timeStamp);
		handleMessage(message);
		//System.out.println();
		
	}
	private void handleMessage(MidiMessage message) {
		if (message instanceof MetaMessage) {
			handleMetaMessage((MetaMessage) message);
		} else if (message instanceof ShortMessage) {
			handleShortMessage((ShortMessage) message);
		}
		else{
			System.out.print(message.toString());
		}
	}

	private void handleShortMessage(ShortMessage message) {
		int noteNumber;
		int velocity;
		//System.out.print(" Channel:"+ message.getChannel());
		switch (message.getCommand()) {
		default:
			System.out.print(" Command "+Integer.toHexString(message.getCommand()));
			break;
		case Commands.NOTE_ON:
			System.out.print(" Channel:"+ message.getChannel());
			noteNumber = message.getData1();
			velocity = message.getData2();
			//channels[message.getChannel()].noteOn(noteNumber, velocity);
			System.out.println(" "+message.getCommand() +" note:" + noteNumber + " Velocity:" + velocity);
			break;
		case Commands.NOTE_OFF:
			System.out.print(" Channel:"+ message.getChannel());
			noteNumber = message.getData1();
			velocity = message.getData2();
			//channels[message.getChannel()].noteOff(noteNumber, velocity);
			System.out.println(" "+message.getCommand() +" note:" + noteNumber + " Velocity:" + velocity);
			break;
		case 0xf0:
			break;
		}
	}

	private void handleMetaMessage(MetaMessage message) {
		switch (message.getType()) {
		default:
			System.out.println(" Type:"+Integer.toHexString(message.getType()));
			break;
		case MetaMessageType.SET_TEMPO:
			byte[] data = message.getData();
			int tempo = (data[0] & 0xff) << 16 | (data[1] & 0xff) << 8 | (data[2] & 0xff);
			//bpm = 60000000 / tempo;
			//tickDuration = (60000.0 / (bpm * resolution));
			//System.out.println(" Tempo " + tempo + " Bpm " + bpm);
			break;
		case MetaMessageType.TEXT:
			System.out.println(" Text:"+new String(message.getData()));
			break;
		case MetaMessageType.COPYRIGHT_NOTICE:
			System.out.println(" Copyright " + new String(message.getData()));
			break;
		case MetaMessageType.INSTRUMENT_NAME:
			System.out.println(" Instrument " + new String(message.getData()));
			break;
		case MetaMessageType.TRACK_NAME:
			System.out.println(" Track Name:"+new String(message.getData()));
			break;
		}

	}

}
