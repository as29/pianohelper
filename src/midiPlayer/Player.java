package midiPlayer;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.sound.midi.*;
import javax.swing.plaf.synth.SynthEditorPaneUI;

public class Player {
	private Boolean isRunning=false;
	private File CurrentSong;
	private Synthesizer synthesizer;
	private Sequencer sequencer;
	private Soundbank soundbank;
	public Player() throws MidiUnavailableException, InvalidMidiDataException, IOException
	{
		synthesizer= MidiSystem.getSynthesizer();
		//soundbank=MidiSystem.getSoundbank(new File("./sf2/GS.sf2"));
		soundbank = synthesizer.getDefaultSoundbank();
		synthesizer.loadAllInstruments(soundbank);
		sequencer=  MidiSystem.getSequencer();
		sequencer.open();
	}
	
	public Soundbank getSoundbank() {
		return soundbank;
	}
	public void setSoundbank(Soundbank soundbank) throws MidiUnavailableException {
		this.soundbank = soundbank;
		sequencer.close();
		synthesizer.loadAllInstruments(soundbank);
		sequencer.open();
	}
	public void setSoundbank( File file) throws InvalidMidiDataException, IOException, MidiUnavailableException {
		this.soundbank = MidiSystem.getSoundbank(file);
		sequencer.close();
		synthesizer.loadAllInstruments(soundbank);
		sequencer.open();
	}
	public File getCurrentSong() {
		return CurrentSong;
	}
	public void setCurrentSong(File currentSong) throws InvalidMidiDataException, IOException {
		CurrentSong = currentSong;
		sequencer.setSequence(MidiSystem.getSequence(currentSong));
	}
	public void start() {
		isRunning=true;
		sequencer.start();
	}
	public void stop() {
		isRunning=false;
		sequencer.stop();
	}
	
	protected void finalize(){
		if(sequencer.isOpen())
		{
			sequencer.close();
			System.out.println("sequencer closed");
		};
	}

	public void close() {
		sequencer.close();
		
	}
}
