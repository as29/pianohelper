package source;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import midiPlayer.Player;
public class Main {
	public static void clrscr(){
	    //Clears Screen in java
	    try {
	        if (System.getProperty("os.name").contains("Windows"))
	            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
	        else
	            Runtime.getRuntime().exec("clear");
	    } catch (IOException | InterruptedException ex) {}
	}
    
    public static void main(String[] args) throws InvalidMidiDataException, IOException, MidiUnavailableException, InterruptedException {
		List<Path> paths = new ArrayList<Path>();
		Scanner in = new Scanner(System.in);
		try (Stream<Path> filePathStream = Files.walk(Paths.get("Midi"))) {
			filePathStream.forEach(filePath -> {
				if (Files.isRegularFile(filePath)) {
					paths.add(filePath);
				}
			});
		}
		int track;
		for (int index = 0; index < paths.size(); ++index) {
			System.out.println("[" + index + "] " + paths.get(index).toString());
		}
		MidiDevice.Info[] info=MidiSystem.getMidiDeviceInfo();
		for(int index=0;index<info.length;index++)
		{
			System.out.println("["+index+"]Name:"+info[index].getName()+" Description: "+info[index].getDescription()+" Vendor:"+info[index].getVendor());
		}
		//int IndexDevice=in.nextInt();
		//MidiDevice device=MidiSystem.getMidiDevice(info[IndexDevice]);
		Player player=new Player();
		int option=1;
		while(option!=0) {
			option=in.nextInt();
			switch(option) {
			case 1:			
				track=in.nextInt();
				player.setCurrentSong(paths.get(track).toFile());
				break;
			case 2:
				player.start();
				break;
			case 3:
				player.stop();
				break;
			case 4:
				String string=in.next();
				string="./sf2/"+string+".sf2";
				System.out.println(string);
				player.setSoundbank(new File(string));
			case 0:
				player.close();
				break;
			}
		//int soundFont=in.nextInt();
		}
	}

}
